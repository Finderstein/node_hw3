/* eslint-disable require-jsdoc, camelcase */
import {StatusCodes} from 'http-status-codes';

export function checkRoleDriver(req, res, next) {
  const user = req.user;

  if (user.role !== 'DRIVER') {
    return res.status(StatusCodes.BAD_REQUEST).json({
      message: 'Invalid user role',
    });
  }

  next();
}

export function checkRoleShipper(req, res, next) {
  const user = req.user;

  if (user.role !== 'SHIPPER') {
    return res.status(StatusCodes.BAD_REQUEST).json({
      message: 'Invalid user role',
    });
  }

  next();
}
