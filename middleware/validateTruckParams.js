/* eslint-disable require-jsdoc, camelcase */
import validate from '../validation/validator.js';
import {StatusCodes} from 'http-status-codes';
import truckSchema from '../validation/truckSchema.js';

export function checkTruckType(req, res, next) {
  const {type} = req.body;
  if (!type) {
    return res.status(StatusCodes.BAD_REQUEST).json({
      message: 'Not enough parameters. Must have \'type\'',
    });
  }
  validate({type}, truckSchema, res, next);
}

export function checkTruckId(req, res, next) {
  const {id} = req.params;

  if (!id) {
    res
        .status(StatusCodes.BAD_REQUEST)
        .json({message: 'Url must have id path parameter'});
  }
  validate({id}, truckSchema, res, next);
}
