/* eslint-disable require-jsdoc, camelcase */
import validate from '../validation/validator.js';
import {StatusCodes} from 'http-status-codes';
import loadSchema from '../validation/loadSchema.js';

export function checkLoadInfo(req, res, next) {
  const {name, payload, pickup_address, delivery_address, dimensions} =
    req.body;
  if (
    !name ||
    !payload ||
    !pickup_address ||
    !delivery_address ||
    !dimensions
  ) {
    return res.status(StatusCodes.BAD_REQUEST).json({
      message:
        'Not enough parameters. Must have \'name\', \'payload\',' +
        ' \'pickup_address\', \'delivery_address\', \'dimensions\'',
    });
  }
  validate(
      {name, payload, pickup_address, delivery_address, dimensions},
      loadSchema,
      res,
      next,
  );
}

export function checkLoadPaginationAndStatus(req, res, next) {
  const {status, limit, offset} = req.query;
  const data = {};

  if (status) {
    data.status = status;
  }
  if (limit) {
    data.limit = limit;
  }
  if (offset) {
    data.offset = offset;
  }

  validate(data, loadSchema, res, next);
}

export function checkLoadId(req, res, next) {
  const {id} = req.params;

  if (!id) {
    res
        .status(StatusCodes.BAD_REQUEST)
        .json({message: 'Url must have id path parameter'});
  }
  validate({id}, loadSchema, res, next);
}
