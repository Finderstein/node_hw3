/* eslint-disable require-jsdoc, camelcase */
import User from '../models/user.model.js';
import Truck from '../models/truck.model.js';
import bcrypt from 'bcryptjs';
import { StatusCodes } from 'http-status-codes';

const saltRounds = 10;

export function userProfile(req, res) {
  const user = req.user;

  res.status(StatusCodes.OK).json({
    user: {
      _id: user._id,
      role: user.role,
      email: user.email,
      createdDate: user.createdDate
    }
  });
}

export async function deleteUser(req, res) {
  const user = req.user;

  try {
    if (user.role === 'DRIVER') {
      const truck = await Truck.getByAssigned(user._id);
      if (truck) {
        truck.assigned_to = '';
        await Truck.update(truck);
      }
    }

    await User.delete(user._id);

    res.status(StatusCodes.OK).json({
      message: 'Success'
    });
  } catch (err) {
    res
      .status(StatusCodes.INTERNAL_SERVER_ERROR)
      .json({ message: 'Internal server error' });
  }
}

export async function updateUserPass(req, res) {
  const user = req.user;
  const { newPassword } = req.body;

  try {
    user.password = await bcrypt.hash(newPassword, saltRounds);
    await User.update(user);
    res.status(StatusCodes.OK).json({
      message: 'Password changed successfully'
    });
  } catch (err) {
    res
      .status(StatusCodes.INTERNAL_SERVER_ERROR)
      .json({ message: 'Internal server error' });
  }
}
