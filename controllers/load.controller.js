/* eslint-disable require-jsdoc, camelcase */
import { StatusCodes } from "http-status-codes";
import Load from "../models/load.model.js";
import nextLoadState from "../auxiliary/nextLoadState.js";
import Truck from "../models/truck.model.js";
import TruckTypes from "../auxiliary/TruckTypes.js";

const LIMIT_DEFAULT = 10;

export async function allLoads(req, res) {
	const user = req.user;
	const { status, limit, offset } = req.query;

	try {
		let loads = [];
		if (user.role === "DRIVER") {
			loads = await Load.getByAssignedWithPagination(
				user._id,
				limit ? +limit : LIMIT_DEFAULT,
				offset ? +offset : 0
			);
		} else {
			loads = await Load.getByCreatorWithPagination(
				user._id,
				limit ? +limit : LIMIT_DEFAULT,
				offset ? +offset : 0
			);
		}

		if (status) {
			loads = loads.filter((load) => load.status === status);
		}

		res.status(StatusCodes.OK).json({ loads });
	} catch (err) {
		res
			.status(StatusCodes.INTERNAL_SERVER_ERROR)
			.json({ message: "Internal server error" });
	}
}

export async function addLoad(req, res) {
	const user = req.user;
	const { name, payload, pickup_address, delivery_address, dimensions } =
		req.body;

	try {
		await Load.insert(
			new Load(
				user._id,
				name,
				payload,
				pickup_address,
				delivery_address,
				dimensions
			)
		);

		res.status(StatusCodes.OK).json({ message: "Load created successfully" });
	} catch (err) {
		res
			.status(StatusCodes.INTERNAL_SERVER_ERROR)
			.json({ message: "Internal server error" });
	}
}

export async function activeLoad(req, res) {
	const user = req.user;

	try {
		const load = await Load.getDriverActive(user._id);

		if (!load) {
			return res.status(StatusCodes.OK).json({ message: "No active load" });
		}

		res.status(StatusCodes.OK).json({ load });
	} catch (err) {
		res
			.status(StatusCodes.INTERNAL_SERVER_ERROR)
			.json({ message: "Internal server error" });
	}
}

export async function iterateActiveLoadState(req, res) {
	const user = req.user;

	try {
		const load = await Load.getDriverActive(user._id);

		if (!load) {
			return res
				.status(StatusCodes.BAD_REQUEST)
				.json({ message: "No active load" });
		}
		if (load.state === "Arrived to delivery") {
			return res
				.status(StatusCodes.BAD_REQUEST)
				.json({ message: "Load is already delivered" });
		}

		const nextState = nextLoadState(load.state);

		if (nextState === "Arrived to delivery") {
			load.status = "SHIPPED";
			const truck = await Truck.getByAssigned(load.assigned_to);
			truck.status = "IS";
			await Truck.update(truck);
		}

		load.logs.push({
			message: "Load status changed to " + nextState,
			time: new Date().toISOString(),
		});

		load.state = nextState;
		await Load.update(load);

		res
			.status(StatusCodes.OK)
			.json({ message: `Load state changed to '${nextState}'` });
	} catch (err) {
		res
			.status(StatusCodes.INTERNAL_SERVER_ERROR)
			.json({ message: "Internal server error" });
	}
}

export async function getLoad(req, res) {
	const { id } = req.params;

	try {
		const load = await Load.getById(id);

		if (!load) {
			return res
				.status(StatusCodes.BAD_REQUEST)
				.json({ message: "No load with this id" });
		}

		res.status(StatusCodes.OK).json({ load });
	} catch (err) {
		res
			.status(StatusCodes.INTERNAL_SERVER_ERROR)
			.json({ message: "Internal server error" });
	}
}

export async function updateLoad(req, res) {
	const { id } = req.params;
	const { name, payload, pickup_address, delivery_address, dimensions } =
		req.body;

	try {
		const load = await Load.getById(id);
		if (!load) {
			return res
				.status(StatusCodes.BAD_REQUEST)
				.json({ message: "No load with this id" });
		}

		if (load.status !== "NEW") {
			return res
				.status(StatusCodes.BAD_REQUEST)
				.json({ message: `Load doesn't have status 'NEW'` });
		}

		load.name = name;
		load.payload = payload;
		load.pickup_address = pickup_address;
		load.delivery_address = delivery_address;
		load.dimensions = dimensions;
		load.logs.push({
			message: "Load params were updated",
			time: new Date().toISOString(),
		});
		await Load.update(load);

		res
			.status(StatusCodes.OK)
			.json({ message: "Load details changed successfully" });
	} catch (err) {
		res
			.status(StatusCodes.INTERNAL_SERVER_ERROR)
			.json({ message: "Internal server error" });
	}
}

export async function deleteLoad(req, res) {
	const { id } = req.params;

	try {
		const load = await Load.getById(id);
		if (!load) {
			return res
				.status(StatusCodes.BAD_REQUEST)
				.json({ message: "No load with this id" });
		}

		if (load.status !== "NEW") {
			return res
				.status(StatusCodes.BAD_REQUEST)
				.json({ message: `Load doesn't have status 'NEW'` });
		}

		await Load.delete(id);

		res.status(StatusCodes.OK).json({ message: "Load deleted successfully" });
	} catch (err) {
		res
			.status(StatusCodes.INTERNAL_SERVER_ERROR)
			.json({ message: "Internal server error" });
	}
}

export async function postLoad(req, res) {
	const { id } = req.params;

	try {
		const load = await Load.getById(id);
		if (!load) {
			return res
				.status(StatusCodes.BAD_REQUEST)
				.json({ message: "No load with this id" });
		}

		if (load.status !== "NEW") {
			return res
				.status(StatusCodes.BAD_REQUEST)
				.json({ message: `Load already posted` });
		}

		load.status = "POSTED";
		load.logs.push({
			message: "Load posted",
			time: new Date().toISOString(),
		});
		await Load.update(load);

		const dimensionsArr = [
			load.dimensions.width,
			load.dimensions.length,
			load.dimensions.height,
		];
		dimensionsArr.sort((a, b) => b - a);

		let driver_found = false;
		const trucks = await Truck.getIS();
		for (const truck of trucks) {
			const truckTypeParams = TruckTypes[truck.type];

			if (
				truck.status === "OL" ||
				truck.assigned_to === "" ||
				truckTypeParams.carrying_capacity < load.payload ||
				dimensionsArr.some(
					(dimension, i) => truckTypeParams.dimensions[i] < dimension
				)
			) {
				continue;
			} else {
				truck.status = "OL";
				await Truck.update(truck);

				load.status = "ASSIGNED";
				load.state = nextLoadState(load.state);
				load.assigned_to = truck.assigned_to;
				load.logs.push({
					message: "Load assigned to driver with id " + truck.assigned_to,
					time: new Date().toISOString(),
				});
				await Load.update(load);

				driver_found = true;

				break;
			}
		}

		if (!driver_found) {
			load.status = "NEW";
			load.logs.push({
				message: "Cannot find suitable truck and driver, posting failed",
				time: new Date().toISOString(),
			});
			await Load.update(load);
		}

		res.status(StatusCodes.OK).json({
			message: driver_found
				? "Load posted successfully"
				: "Load posting failed",
			driver_found,
		});
	} catch (err) {
		res
			.status(StatusCodes.INTERNAL_SERVER_ERROR)
			.json({ message: "Internal server error" });
	}
}

export async function getLoadShippingInfo(req, res) {
	const { id } = req.params;

	try {
		const load = await Load.getById(id);
		if (!load) {
			return res
				.status(StatusCodes.BAD_REQUEST)
				.json({ message: "No load with this id" });
		}

		if (load.assigned_to === "") {
			return res.status(StatusCodes.OK).json({ message: `Load is not active` });
		}

		const truck = await Truck.getByAssigned(load.assigned_to);

		if (!truck) {
			return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
				message:
					"Truck with assigned driver that has id " +
					load.assigned_to +
					" not found",
			});
		}

		res.status(StatusCodes.OK).json({ load, truck });
	} catch (err) {
		res
			.status(StatusCodes.INTERNAL_SERVER_ERROR)
			.json({ message: "Internal server error" });
	}
}
