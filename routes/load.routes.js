import express from 'express';
import verifyToken from '../middleware/authJwt.js';
import {
  allLoads,
  addLoad,
  activeLoad,
  iterateActiveLoadState,
  getLoad,
  updateLoad,
  deleteLoad,
  postLoad,
  getLoadShippingInfo,
} from '../controllers/load.controller.js';
import {
  checkRoleDriver,
  checkRoleShipper,
} from '../middleware/validateUserRole.js';
import {
  checkLoadId,
  checkLoadPaginationAndStatus,
  checkLoadInfo,
} from '../middleware/validateLoadParams.js';

const router = new express.Router();

router.get('/', [verifyToken, checkLoadPaginationAndStatus], allLoads);
router.post('/', [verifyToken, checkRoleShipper, checkLoadInfo], addLoad);
router.get('/active', [verifyToken, checkRoleDriver], activeLoad);
router.patch(
    '/active/state',
    [verifyToken, checkRoleDriver],
    iterateActiveLoadState,
);
router.get('/:id', [verifyToken, checkLoadId], getLoad);
router.put(
    '/:id',
    [verifyToken, checkRoleShipper, checkLoadId, checkLoadInfo],
    updateLoad,
);
router.delete('/:id', [verifyToken, checkRoleShipper, checkLoadId], deleteLoad);
router.post(
    '/:id/post',
    [verifyToken, checkRoleShipper, checkLoadId],
    postLoad,
);
router.get(
    '/:id/shipping_info',
    [verifyToken, checkRoleShipper, checkLoadId],
    getLoadShippingInfo,
);

export default router;
