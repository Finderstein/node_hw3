import express from 'express';
import verifyToken from '../middleware/authJwt.js';
import {
  userProfile,
  deleteUser,
  updateUserPass,
} from '../controllers/user.controller.js';
import {checkValidNewPass} from '../middleware/validateAuthParams.js';

const router = new express.Router();

router.get('/me', [verifyToken], userProfile);
router.delete('/me', [verifyToken], deleteUser);
router.patch('/me/password', [verifyToken, checkValidNewPass], updateUserPass);

export default router;
