/* eslint-disable require-jsdoc, camelcase */
export default function nextLoadState(state) {
  switch (state) {
    case 'En route to Pick Up':
      return 'Arrived to Pick Up';
    case 'Arrived to Pick Up':
      return 'En route to delivery';
    case 'En route to delivery':
      return 'Arrived to delivery';
    default:
      return 'En route to Pick Up';
  }
}
