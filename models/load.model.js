/* eslint-disable require-jsdoc, camelcase */
import mongoose from 'mongoose';

const LoadShema = new mongoose.Schema(
    {
      created_by: {type: String, required: true},
      assigned_to: {type: String},
      status: {type: String, required: true},
      state: {type: String},
      name: {type: String, required: true},
      payload: {type: Number, required: true},
      pickup_address: {type: String, required: true},
      delivery_address: {type: String, required: true},
      dimensions: {
        width: {type: Number, required: true},
        length: {type: Number, required: true},
        height: {type: Number, required: true},
      },
      logs: [
        {
          message: {type: String},
          time: {type: String},
          _id: false,
        },
      ],
      createdDate: {type: String, required: true},
    },
    {versionKey: false},
);

const LoadModel = mongoose.model('Load', LoadShema);

class Load {
  constructor(
      created_by,
      name,
      payload,
      pickup_address,
      delivery_address,
      dimensions,
  ) {
    this.created_by = created_by;
    this.assigned_to = '';
    this.status = 'NEW';
    this.state = '';
    this.name = name;
    this.payload = payload;
    this.pickup_address = pickup_address;
    this.delivery_address = delivery_address;
    this.dimensions = dimensions;
    this.logs = [];
    this.createdDate = new Date().toISOString();
  }

  static insert(truck) {
    return new LoadModel(truck).save();
  }

  static getById(id) {
    return LoadModel.findById({_id: id});
  }

  static getAll() {
    return LoadModel.find();
  }

  static getDriverActive(assigned_to) {
    return LoadModel.findOne({assigned_to, status: 'ASSIGNED'});
  }

  static getByCreatorWithPagination(created_by, limit, offset) {
    return LoadModel.find({created_by}).skip(offset).limit(limit);
  }

  static getByAssignedWithPagination(assigned_to, limit, offset) {
    return LoadModel.find({assigned_to}).skip(offset).limit(limit);
  }

  static update(truck) {
    return LoadModel.findByIdAndUpdate(truck._id, truck);
  }

  static delete(id) {
    return LoadModel.findByIdAndDelete(id);
  }
}

export default Load;
