/* eslint-disable require-jsdoc, camelcase */
import mongoose from 'mongoose';

const TruckShema = new mongoose.Schema(
    {
      created_by: {type: String, required: true},
      assigned_to: {type: String},
      type: {type: String, required: true},
      status: {type: String, required: true},
      createdDate: {type: String, required: true},
    },
    {versionKey: false},
);

const TruckModel = mongoose.model('Truck', TruckShema);

class Truck {
  constructor(created_by, type) {
    this.created_by = created_by;
    this.assigned_to = '';
    this.type = type;
    this.status = 'IS';
    this.createdDate = new Date().toISOString();
  }

  static insert(truck) {
    return new TruckModel(truck).save();
  }

  static getById(id) {
    return TruckModel.findById({_id: id});
  }

  static getAll() {
    return TruckModel.find();
  }

  static getIS() {
    return TruckModel.find({status: 'IS'});
  }

  static getByAssigned(assigned_to) {
    return TruckModel.findOne({assigned_to});
  }

  static update(truck) {
    return TruckModel.findByIdAndUpdate(truck._id, truck);
  }

  static delete(id) {
    return TruckModel.findByIdAndDelete(id);
  }
}

export default Truck;
