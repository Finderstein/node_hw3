import Joi from 'joi';

export default Joi.object({
  email: Joi.string().email({minDomainSegments: 2, tlds: {allow: false}}),
  password: Joi.string().pattern(new RegExp('^[a-zA-Z0-9]{3,255}$')),
  role: Joi.string().valid('SHIPPER', 'DRIVER'),
});
